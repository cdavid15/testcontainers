package uk.co.cd15.testcontainers.mongo;

import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import java.util.Arrays;

import static org.rnorth.visibleassertions.VisibleAssertions.*;

/**
 * Tests for MongoDBTest
 */
public class MongoDBTest {


    private static final int MONGO_PORT = 27017;

    /**
     * MongoDB
     */
    @ClassRule
    public static GenericContainer mongo = new GenericContainer("mongo:4.2.0")
            .withExposedPorts(MONGO_PORT);


    @Test
    public void simpleMongoDbTest() {
        MongoClient mongoClient = MongoClients.create(
                MongoClientSettings.builder()
                        .applyToClusterSettings(builder ->
                                builder.hosts(Arrays.asList(new ServerAddress(mongo.getContainerIpAddress(), mongo.getMappedPort(MONGO_PORT)))))
                        .build());
        MongoDatabase database = mongoClient.getDatabase("test");
        MongoCollection<Document> collection = database.getCollection("testCollection");

        Document doc = new Document("name", "foo")
                .append("value", 1);
        collection.insertOne(doc);

        Document doc2 = collection.find(new Document("name", "foo")).first();
        assertEquals("A record can be inserted into and retrieved from MongoDB", 1, doc2.get("value"));
    }

}


