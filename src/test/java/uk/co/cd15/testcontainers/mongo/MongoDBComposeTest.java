package uk.co.cd15.testcontainers.mongo;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.io.File;
import java.time.Duration;
import java.util.Arrays;

import static org.rnorth.visibleassertions.VisibleAssertions.*;

/**
 * Tests for MongoDBTest
 */
public class MongoDBComposeTest {


    private static final int MONGO_PORT = 27017;

    @ClassRule
    public static DockerComposeContainer environment =
            new DockerComposeContainer(new File("src/test/resources/compose-test.yml"))
                    .withExposedService("mongo_1", MONGO_PORT,
                            Wait.forListeningPort().withStartupTimeout(Duration.ofSeconds(30)));


    @Test
    public void simpleMongoDbTest() {
        String user = "root";     // the user name
        String source = "admin";   // the source where the user is defined
        char[] password = "example".toCharArray(); // the password as a character array

        MongoCredential credential = MongoCredential.createCredential(user, source, password);


        MongoClient mongoClient = MongoClients.create(
                MongoClientSettings.builder()
                        .applyToClusterSettings(builder ->
                                builder.hosts(Arrays.asList(new ServerAddress(
                                        environment.getServiceHost("mongo_1", MONGO_PORT),
                                        environment.getServicePort("mongo_1", MONGO_PORT)
                                        ))))
                        .credential(credential)
                        .build());
        MongoDatabase database = mongoClient.getDatabase("test");
        MongoCollection<Document> collection = database.getCollection("testCollection");

        Document doc = new Document("name", "foo")
                .append("value", 1);
        collection.insertOne(doc);

        Document doc2 = collection.find(new Document("name", "foo")).first();
        assertEquals("A record can be inserted into and retrieved from MongoDB", 1, doc2.get("value"));
    }

}


